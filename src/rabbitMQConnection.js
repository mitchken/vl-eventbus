const amqp = require('amqplib/callback_api');

let amqpConn = null;
let pubChannel = null;
const offlinePubQueue = [];
let failures = 0;

// Connects to the RabbitMQ server
const start = () => {
  amqp.connect(
    process.env.RABBITMQ_CONNECTION_STRING,
    function(err, conn) {
      if (err) {
        console.error('[AMQP]', err.message);
        failures += 1;
        if (failures < 10) {
          setTimeout(start, 10000);
          return;
        }
        console.log(`Stopped connection to rabbitMQ after ${failures} retries`);
        return;
      }
      conn.on('error', function(e) {
        if (err.message !== 'Connection closing') {
          console.error('[AMQP] conn error', e.message);
        }
      });
      conn.on('close', function() {
        console.error('[AMQP] reconnecting');
        setTimeout(start, 1000);
      });
      console.log('[AMQP] connected');
      amqpConn = conn;
      whenConnected();
    },
  );
};

// Default error function, will fully close the connection to rabbitMQ
const closeOnErr = err => {
  if (!err) return false;
  console.error('[AMQP] error', err);
  amqpConn.close();
  return true;
};

const whenConnected = () => {
  amqpConn.createConfirmChannel(function(err, ch) {
    if (closeOnErr(err)) return;
    ch.on('error', function(e) {
      console.error('[AMQP] channel error', e.message);
    });
    ch.on('close', function() {
      console.log('[AMQP] channel closed');
    });

    pubChannel = ch;
    while (true) {
      const m = offlinePubQueue.shift();
      if (!m) break;
      publish(m[0], m[1], m[2]);
    }
  });
};

// publicshed messages from the queue to amqp
const publish = (exchange, routingKey, content) => {
  try {
    pubChannel.publish(
      exchange,
      routingKey,
      content,
      { persistent: true },
      err => {
        if (err) {
          console.error('[AMQP] publish', err);
          offlinePubQueue.push([exchange, routingKey, content]);
          pubChannel.connection.close();
        } else {
          // message was successfully published
        }
      },
    );
  } catch (e) {
    console.error('[AMQP] publish', e.message);
    offlinePubQueue.push([exchange, routingKey, content]);
  }
};

/**
 * Sends an update to rabbitMQ
 * @param action String 'create'|'update'|'delete'
 * @param type String Name of the collection to update
 * @param content Object Content of the object to undex
 * @param searcheableFields [String] Fields to pick from the content which will be indexed
 */
const send = (action, type, content) => {
  publish(
    process.env.TOPIC_EXCHANGE,
    `${type}.${action}`,
    Buffer.from(JSON.stringify({ action, type, content })),
  );
};

start();

module.exports = { send };
